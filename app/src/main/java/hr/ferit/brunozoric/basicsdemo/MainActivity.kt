package hr.ferit.brunozoric.basicsdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main_linear.*

class MainActivity : AppCompatActivity() {

    private val TAG: String = "BasicsDemo"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_linear)
        setUpUi()
    }

    private fun setUpUi() {
        messageAction.setOnClickListener{ displayMessage() }
    }

    private fun displayMessage() {
        val message: String = messageInput.text.toString()
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        Log.d(TAG, message)
    }
}
